package com.gdaditya.gatewayservice.logging;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LogServiceImpl implements LogService {

    @Value("${spring.application.name}")
    private String serviceName;
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void log(String message) {
        kafkaTemplate.send("log", serviceName+": "+message);
    }

    @Override
    public void logRequest(String ip, String path, String method) {
        log(String.format("request from %s to %s method %s.", ip, path, method));
    }
}
