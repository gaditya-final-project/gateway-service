package com.gdaditya.gatewayservice.logging;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class RequestLogger implements GlobalFilter {

    private final LogService logService;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        logService.logRequest(
                Objects.requireNonNull(exchange.getRequest().getRemoteAddress()).toString(),
                exchange.getRequest().getPath().value(),
                exchange.getRequest().getMethodValue()
        );
        return chain.filter(exchange);
    }
}
