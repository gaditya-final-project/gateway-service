package com.gdaditya.gatewayservice.handler;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;

import java.net.ConnectException;
import java.util.HashMap;
import java.util.Map;

@Component
public class GlobalErrorAttributes extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request,
                                                  ErrorAttributeOptions options) {
        Throwable error = super.getError(request);
        HttpStatus httpStatus = getHttpStatus(error);

        Map<String, Object> map = new HashMap<>();
        map.put("status", httpStatus);
        map.put("message", getMessageFromError(error));
        map.put("success", false);
        return map;
    }

    private String getMessageFromError(Throwable er) {
        if (er instanceof ResponseStatusException){
            ResponseStatusException er1 = (ResponseStatusException) er;
            if (er1.getStatus().equals(HttpStatus.NOT_FOUND))
                return "No handler found for requested URI";
            return ((ResponseStatusException) er).getReason();
        }
        else if (er instanceof ConnectException)
            return "Failed to communicate with service.";
        else
            return er.getMessage();
    }

    private HttpStatus getHttpStatus(Throwable error) {
        MergedAnnotation<ResponseStatus> responseStatusAnnotation = MergedAnnotations.from(error.getClass(), MergedAnnotations.SearchStrategy.TYPE_HIERARCHY).get(ResponseStatus.class);
        return error instanceof ResponseStatusException ? ((ResponseStatusException)error).getStatus() : responseStatusAnnotation.getValue("code", HttpStatus.class).orElse(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}