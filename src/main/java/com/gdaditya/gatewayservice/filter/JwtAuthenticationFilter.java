package com.gdaditya.gatewayservice.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gdaditya.gatewayservice.exception.JwtTokenMalformedException;
import com.gdaditya.gatewayservice.exception.JwtTokenMissingException;
import com.gdaditya.gatewayservice.model.dto.BaseResponse;
import com.gdaditya.gatewayservice.util.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.function.Predicate;

@Log4j2
@Component
public class JwtAuthenticationFilter implements GatewayFilter {

    @Autowired
    private JwtUtil jwtUtil;

    private Mono<Void> getResponseException(String message, HttpStatus httpStatus, ServerWebExchange exchange) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);

        DataBufferFactory dataBufferFactory = exchange.getResponse().bufferFactory();
        ObjectMapper objMapper = new ObjectMapper();
        byte[] obj;
        try {
            obj = objMapper.writeValueAsBytes(new BaseResponse<Object>(false, httpStatus.value(), message, null));
            return exchange.getResponse().writeWith(Mono.just(obj).map(dataBufferFactory::wrap));
        } catch (JsonProcessingException e) {
            log.error(e);
        }
        return exchange.getResponse().setComplete();
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();

        final List<String> apiEndpoints = List.of("/register", "/login");

        Predicate<ServerHttpRequest> isApiSecured = r -> apiEndpoints.stream()
                .noneMatch(uri -> r.getURI().getPath().contains(uri));

        if (isApiSecured.test(request)) {
            if (!request.getHeaders().containsKey("Authorization")) {
                return getResponseException("Authorization token required!", HttpStatus.UNAUTHORIZED, exchange);
            }

            final String token = request.getHeaders().getOrEmpty("Authorization").get(0);
            if (!token.startsWith("Bearer "))
                return getResponseException("Invalid Authorization token!", HttpStatus.UNAUTHORIZED, exchange);


            try {
                jwtUtil.validateToken(token.substring(7));
            } catch (JwtTokenMalformedException | JwtTokenMissingException e) {
                log.warn(e);
                return getResponseException(e.getMessage(), HttpStatus.UNAUTHORIZED, exchange);
            }
        }

        return chain.filter(exchange);
    }

}
