package com.gdaditya.gatewayservice.config;

import com.gdaditya.gatewayservice.filter.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Autowired
    private JwtAuthenticationFilter filter;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("auth-service", r -> r.path("/auth/**", "/api/user/**").filters(f -> f.filter(filter)).uri("lb://auth-service"))
                .route("training-service", r -> r.path("/api/preferences/**", "/api/training/**", "/api/enrollment/**").filters(f -> f.filter(filter)).uri("lb://training-service"))
                .route("portfolio-service", r -> r.path("/api/portfolio/**").filters(f -> f.filter(filter)).uri("lb://portfolio-service"))
                .build();
    }

}
