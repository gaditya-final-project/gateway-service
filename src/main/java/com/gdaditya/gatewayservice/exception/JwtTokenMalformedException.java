package com.gdaditya.gatewayservice.exception;

import javax.naming.AuthenticationException;

/**
 * Created by IntelliJ IDEA.
 * Project : api-gateway
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/12/21
 * Time: 07.42
 */

public class JwtTokenMalformedException extends AuthenticationException {

    private static final long serialVersionUID = 1L;

    public JwtTokenMalformedException(String msg) {
        super(msg);
    }

}
