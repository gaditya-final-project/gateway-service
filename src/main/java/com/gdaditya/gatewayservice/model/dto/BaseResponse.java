package com.gdaditya.gatewayservice.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseResponse<T> {
    private Boolean success = true;
    @JsonIgnore
    private int messageCode = 200;
    private String message = "Operation success";
    private T data;

    public BaseResponse(T data) {
        this.data = data;
    }

    public BaseResponse(boolean success, int messageCode, String message, T data) {
        this.success = success;
        this.messageCode = messageCode;
        this.message = message;
        this.data = data;
    }
}
